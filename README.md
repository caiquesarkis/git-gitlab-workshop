---
Autor: Caique Sarkis
Data : 23/05/2021
Descrição: Tutorial básico de Git e Gitlab
---

# Tutorial básico de Git e Gitlab



## Git 
- Git é um sistema de controle de versão. Ele armazena todas as alterações feitas no projeto e cria versões que funcionam como um histórico. Hoje em dia o Git é o sistema de versionamento mais utilizado no mundo e foi criado pelo Linus Torvalds, o famoso pelo desenvolvimento do sistema operacional Linux. 

- Esse sistema é muito utilizado por programadores, no entanto não é limitado a esste nicho. É possível versionar e proteger arquivos de texto em geral. 

### Instalando git

Para realizar a instalação do Git acesse esse [link](https://git-scm.com/download/win)

Após, aperte a tecla `Win` e escreva **cmd**, em seguida aperte a tecla `enter`.

No terminal execute o seguinte comando

``` console
git version
```

Você deve ver uma mensagem parecida com

``` console
git version 2.28.0.windows.1
```
Isso indica em qual **versão** do software git estamos, isso é bem meta.


O proximo comando é 

``` console
git config --global user.name "write your name here"
```
Você trocar o texto  `write your name here` pelo seu nome. Isso irá configurar seu nome de usuário no sistema.

Um outro comando necessário e bem próximo do anterior é


``` console
git config --global user.email "write your email here"
```
Novamente deve ser trocado o texto entre aspas, neste caso colocando seu email no local. É aconselhavel que esse email seja utilizado em todo o processo desse tutorial.

Um último passo, porém dispensável, é verificar se o usuário está configurado, para isso faça
``` console
git config --global -l 
```

Todos os comandos estão na tabela no final da página.
## Gitlab

### Criando seu primeiro projeto

**Gitlab** é um gerenciador de repositórios(pastas) de software baseado em git. Esse software permite **contribuição remota** em projetos, além de todas as vantagens do git. Existem outras opções no mercado como o **Github** , no entanto este não permite o armazenamento dos projetos em servidores locais o que é possível com o **Gitlab**.

Para criar uma conta no Gitlab acesse esse [link](https://gitlab.com/users/sign_up) 

Após o cadastro, você irá se encontrar na seguinte página.

![gitlab](gitlab1.png)

 Clique em `New project`.
 
 Em seguida, escolha a opção `Create a blank repository`
 
![gitlab](gitlab2.png)

Preencha os campos com o nome do projeto e descrição, defina em `Visibility Level` como `Public` e marque a opção `Initialize repository with a README` . Por fim, clique em  `Create project `

![gitlab](gitlab3.png)

Parabéns, você criou seu primeiro projeto no Gitlab. Vamos entender o que cada componente da interface significa. Na pratica alguns poucos serão utilizados e estarei focando neles.

O primeiro elemento, diz respeito ao título do projeto e sua descrição. Os outros elementos podem ser ignorados por enquanto.

![gitlab](gitlab4.png)

Em seguida pode-se ver a barra de status, referente a ultima modificação feita no repositório. Nela estará contida o nome da modificação e a data.

![gitlab](gitlab5.png)

Logo após, pode-se ver o repositório de fato. Nele constará todos os arquivos e em qual data eles foram modificados.

![gitlab](gitlab6.png)

No final da página temos o `README`. Esta seção é muito importante pois irá educar usuários de fora do seu sistema. Segue um exemplo de um outro projeto que estou realizando

![gitlab](gitlab8.png)

Por fim, o botão de clone. Com ele pode-se trazer o repositório para seu sistema local. Clique em `Clone` em seguida copie o link em `Clone with HTTPS`.

![gitlab](gitlab9.png)

### Repositório local

Volte ao terminal(cmd) e entre no diretório no qual você queira salvar o projeto. Para isso  execute o comando `cd "caminho até o diretório"`. 
```console
cd desktop/projetos
```

O comando `cd` significa `change directory` e , basicamente, muda a pasta na qual você se encontra.

Em seguida, escreva 

``` console
git clone "link copiado"
```

Vá até a pasta no qual você salvou o projeto. Lá você encontrará o arquivo READM.md que é um arquivo markdown, que pode ser modificado em qualquer editor de texto básico. Adicione algo a esse arquivo.


Ok, o arquivo foi modificado mas essa alteração não irá para a nuvem de forma automatica. Para sincronizar com o repositório online deve-se executar os seguintes comandos

```console
git add .
```

Todos os comandos em Git devem começar com `git` seguido do comando.A gente já utilizou esse conceito anteriormente quando fizemos o download do repositório.

Neste caso estamos adicionando as mudanças com o comando `add` e o `.` significa que estamos adicionando tudo que está na pasta.  Em seguida execute 

``` console
git status
```

Para saber se está tudo certo. Neste ponto deverá aparecer o nome dos arquivos em verde no seu terminal.

Logo após, execute

```console 
git commit -m "mensagem"
```

O comando `commit` concretiza as mudanças que você adicionou previamente e associa uma mensagem, através do comando `-m "mensagem"`, para que seja fácil lembrar o que foi feito nesta versão do seu projeto. 

Assim, deve-se mandar tudo para o site do Gitlab, para isso execute 

```console
git push
```

Caso você esteja trabalhando em time é provavel que o documento online seja alterado. Portanto, antes de trabalhar no seu documento deve-se realizar o comando

```console
git push
```

isso irá atualizar seu repositório sem a  necessidade de baixá-lo novamente. Por fim acesse seu projeto no gitlab e veja as alterações.



---
# Cheat Sheet

## Lista de comandos Git
| Comando                                                | Função                                                        |
| ------------------------------------------------------ | ------------------------------------------------------------- |
| git                                                    | Mostra uma lista com todos os comandos disponíveis            |
| git version                                            | Verificar qual versão do git está instalada no seu computador |
| git config --global user.name "write your name here"   | Configura seu nome de usuário                                 |
| git config --global user.email "write your email here" | Configura seu email                                           |
| git config --global -l                                 | Verifica as configurações de usuário atuais                   |



## Lista de comandos Gitlab

| Comando                   | Função                                                                                                     |
| ------------------------- | ---------------------------------------------------------------------------------------------------------- |
| git add "nome do arquivo" | Adiciona arquivos modificados. Caso queira adicioanr todos os arquivos troque o nome do arquivo por um `.` |
| git status                | Verifica o status da operação                                                                              |
| git commit -m "mensagem"  | concretiza as mudanças que você adicionou previamente e associa uma mensagem                               |
| git push                  | Envia todas as alterações para o site do Gitlab                                                            |
| git pull                  | Puxa todas as alterações do site para sua pasta local                                                      |
